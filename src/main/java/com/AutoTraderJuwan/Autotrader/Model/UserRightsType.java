package com.AutoTraderJuwan.Autotrader.Model;

public class UserRightsType {
    private int URId;
    private String RightName;
    private String RightDescription;
    private int RightPrecedence;

    public int getURId() {
        return URId;
    }

    public void setURId(int URId) {
        this.URId = URId;
    }

    public String getRightName() {
        return RightName;
    }

    public void setRightName(String rightName) {
        this.RightName = rightName;
    }

    public String getRightDescription() {
        return RightDescription;
    }

    public void setRightDescription(String rightDescription) {
        this.RightDescription = rightDescription;
    }

    public int getRightPrecedence() {
        return RightPrecedence;
    }

    public void setRightPrecedence(int rightPrecedence) {
        this.RightPrecedence = rightPrecedence;
    }

    public UserRightsType(int URId, String rightName, String rightDescription, int rightPrecedence) {
        this.URId = URId;
        this.RightName = rightName;
        this.RightDescription = rightDescription;
        this.RightPrecedence = rightPrecedence;
    }
}
