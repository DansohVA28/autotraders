package com.AutoTraderJuwan.Autotrader.Model;

public class Condition {
    private int CondId;
    private String ConditionDescription;

    public int getCondId() {
        return CondId;
    }

    public void setCondId(int condId) {
        this.CondId = condId;
    }

    public String getConditionDescription() {
        return ConditionDescription;
    }

    public void setConditionDescription(String conditionDescription) {
        this.ConditionDescription = conditionDescription;
    }

    public Condition(int condId, String conditionDescription) {
        this.CondId = condId;
        this.ConditionDescription = conditionDescription;
    }
}
