package com.AutoTraderJuwan.Autotrader.Model;

public class BodyStyleType {
    private int BId;
    private String Style;

    public int getBId() {
        return BId;
    }

    public void setBId(int BId) {
        this.BId = BId;
    }

    public String getStyle() {
        return Style;
    }

    public void setStyle(String style) {
        this.Style = style;
    }

    public BodyStyleType(int BId, String style) {
        this.BId = BId;
        this.Style = style;
    }
}
