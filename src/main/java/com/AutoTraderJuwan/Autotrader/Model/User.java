package com.AutoTraderJuwan.Autotrader.Model;

public class User {
    private int UserId;
    private String UserName;
    private String UPassword;
    private int UserRights;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        this.UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        this.UserName = userName;
    }

    public String getUPassword() {
        return UPassword;
    }

    public void setUPassword(String UPassword) {
        this.UPassword = UPassword;
    }

    public int getUserRights() {
        return UserRights;
    }

    public void setUserRights(int userRights) {
        this.UserRights = userRights;
    }

    public User(int userId, String userName, String UPassword, int userRights) {
        this.UserId = userId;
        this.UserName = userName;
        this.UPassword = UPassword;
        this.UserRights = userRights;
    }
}
