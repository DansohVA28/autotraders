package com.AutoTraderJuwan.Autotrader.Model;
import java.util.ArrayList;

public class VehicleDetails {
   private int VehicleId;
    private int Mileage;
    private String DriveType;
    private int BodyStyleVechId;
    private int EngingeId;
    private String Transmission;
    private String FuelType;
    private String MPG;
    private int ExId;
    private int InteriorDetailId;
    private ArrayList<String> Images;
    private int CondId;
    private String VDealerInfo;

    public int getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.VehicleId = vehicleId;
    }

    public int getMileage() {
        return Mileage;
    }

    public void setMileage(int mileage) {
        this.Mileage = mileage;
    }

    public String getDriveType() {
        return DriveType;
    }

    public void setDriveType(String driveType) {
        this.DriveType = driveType;
    }

    public int getBodyStyleVechId() {
        return BodyStyleVechId;
    }

    public void setBodyStyleVechId(int bodyStyleVechId) {
        this.BodyStyleVechId = bodyStyleVechId;
    }

    public int getEngingeId() {
        return EngingeId;
    }

    public void setEngingeId(int engingeId) {
        this.EngingeId = engingeId;
    }

    public String getTransmission() {
        return Transmission;
    }

    public void setTransmission(String transmission) {
        this.Transmission = transmission;
    }

    public String getFuelType() {
        return FuelType;
    }

    public void setFuelType(String fuelType) {
        this.FuelType = fuelType;
    }

    public String getMPG() {
        return MPG;
    }

    public void setMPG(String MPG) {
        this.MPG = MPG;
    }

    public int getExId() {
        return ExId;
    }

    public void setExId(int exId) {
        this.ExId = exId;
    }

    public int getInteriorDetailId() {
        return InteriorDetailId;
    }

    public void setInteriorDetailId(int interiorDetailId) {
        this.InteriorDetailId = interiorDetailId;
    }

    public ArrayList<String> getImages() {
        return Images;
    }

    public void setImages(ArrayList<String> images) {
        this.Images = images;
    }

    public int getCondId() {
        return CondId;
    }

    public void setCondId(int condId) {
        this.CondId = condId;
    }

    public String getVDealerInfo() {
        return VDealerInfo;
    }

    public void setVDealerInfo(String VDealerInfo) {
        this.VDealerInfo = VDealerInfo;
    }

    public VehicleDetails(int vehicleId, int mileage, String driveType, int bodyStyleVechId, int engingeId, String transmission, String fuelType, String MPG, int exId, int interiorDetailId, ArrayList<String> images, int condId, String VDealerInfo) {
        VehicleId = vehicleId;
        Mileage = mileage;
        DriveType = driveType;
        BodyStyleVechId = bodyStyleVechId;
        EngingeId = engingeId;
        Transmission = transmission;
        FuelType = fuelType;
        this.MPG = MPG;
        ExId = exId;
        InteriorDetailId = interiorDetailId;
        Images = images;
        CondId = condId;
        this.VDealerInfo = VDealerInfo;
    }
}
