package com.AutoTraderJuwan.Autotrader.Model;

public class InteriorDetails {
    private int InteriorDId;
    private int ColorId;
    private int UpholsteryId;

    public int getInteriorDId() {
        return InteriorDId;
    }

    public void setInteriorDId(int interiorDId) {
        InteriorDId = interiorDId;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        this.ColorId = colorId;
    }

    public int getUpholsteryId() {
        return UpholsteryId;
    }

    public void setUpholsteryId(int upholsteryId) {
        this.UpholsteryId = upholsteryId;
    }

    public InteriorDetails(int interiorDId, int colorId, int upholsteryId) {
        InteriorDId = interiorDId;
        ColorId = colorId;
        UpholsteryId = upholsteryId;
    }
}
