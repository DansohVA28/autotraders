package com.AutoTraderJuwan.Autotrader.Model;

public class Upholstery {
    private int UpholsteryId;
    private int UpholsteryType;

    public int getUpholsteryId() {
        return UpholsteryId;
    }

    public void setUpholsteryId(int upholsteryId) {
        this.UpholsteryId = upholsteryId;
    }

    public int getUpholsteryType() {
        return UpholsteryType;
    }

    public void setUpholsteryType(int upholsteryType) {
        this.UpholsteryType = upholsteryType;
    }

    public Upholstery(int upholsteryId, int upholsteryType) {
        this.UpholsteryId = upholsteryId;
        this.UpholsteryType = upholsteryType;
    }
}
