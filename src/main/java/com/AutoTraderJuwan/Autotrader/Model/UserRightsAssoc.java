package com.AutoTraderJuwan.Autotrader.Model;

public class UserRightsAssoc {
    private int URAId;
    private int UserId;
    private int URId;

    public int getURAId() {
        return URAId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        this.UserId = userId;
    }

    public void setURAId(int URAId) {
        this.URAId = URAId;
    }

    public int getURId() {
        return URId;
    }

    public void setURId(int URId) {
        this.URId = URId;
    }


    public UserRightsAssoc(int URAId, int userId, int URId) {
        this.URAId = URAId;
        this.UserId = userId;
        this.URId = URId;
    }
}
