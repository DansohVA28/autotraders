package com.AutoTraderJuwan.Autotrader.Model;

public class Exterior {
    private int ExId;
    private String ExteriorType;
    private int ColorId;

    public int getExId() {
        return ExId;
    }

    public void setExId(int exId) {
        this.ExId = exId;
    }

    public String getExteriorType() {
        return ExteriorType;
    }

    public void setExteriorType(String exteriorType) {
        this.ExteriorType = exteriorType;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        this.ColorId = colorId;
    }

    public Exterior(int exId, String exteriorType, int colorId) {
        this.ExId = exId;
        this.ExteriorType = exteriorType;
        this.ColorId = colorId;
    }
}
