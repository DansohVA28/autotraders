package com.AutoTraderJuwan.Autotrader.Model;

public class UserListing {
    private int ULId;
    private int UserId;
    private int VehicleID;

    public int getULId() {
        return ULId;
    }

    public void setULId(int ULId) {
        this.ULId = ULId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        this.UserId = userId;
    }

    public int getVehicleID() {
        return VehicleID;
    }

    public void setVehicleID(int vehicleID) {
        this.VehicleID = vehicleID;
    }

    public UserListing(int ULId, int userId, int vehicleID) {
        this.ULId = ULId;
        this.UserId = userId;
        this.VehicleID = vehicleID;
    }
}
