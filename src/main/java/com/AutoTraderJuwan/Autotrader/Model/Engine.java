package com.AutoTraderJuwan.Autotrader.Model;

public class Engine {
    private int EId;
    private String EngineType;

    public int getEId() {
        return EId;
    }

    public void setEId(int EId) {
        this.EId = EId;
    }

    public String getEngineType() {
        return EngineType;
    }

    public void setEngineType(String engineType) {
        this.EngineType = engineType;
    }

    public Engine(int EId, String engineType) {
        this.EId = EId;
        this.EngineType = engineType;
    }
}
