package com.AutoTraderJuwan.Autotrader.Model;

public class Color {
    private int ColorId;
    private String ColorName;

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        this.ColorId = colorId;
    }

    public String getColorName() {
        return ColorName;
    }

    public void setColorName(String colorName) {
        this.ColorName = colorName;
    }

    public Color(int colorId, String colorName) {
        this.ColorId = colorId;
        this.ColorName = colorName;
    }
}
