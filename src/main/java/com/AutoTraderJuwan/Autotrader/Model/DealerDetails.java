package com.AutoTraderJuwan.Autotrader.Model;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class DealerDetails {
    private Long id;
    private String name;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String sellercomment;


    public DealerDetails() {
    }


    public Long getId() {return id; }
    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public String getCity() { return city; }
    public void setCity(String city) { this.city = city; }

    public String getState() { return state; }
    public void setState(String state) { this.state = state; }

    public String getZip() { return zip; }
    public void setZip(String zip) { this.zip = zip; }

    public String getSellercomment() {
        return sellercomment;
    }

    public void setSellercomment(String sellercomment) {
        this.sellercomment = sellercomment;
    }

}
