package com.AutoTraderJuwan.Autotrader.Model;

public class Transmission {
    private int TId;
    private String TransmissionType;

    public int getTId() {
        return TId;
    }

    public void setTId(int TId) {
        this.TId = TId;
    }

    public String getTransmissionType() {
        return TransmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.TransmissionType = transmissionType;
    }

    public Transmission(int TId, String transmissionType) {
        this.TId = TId;
        this.TransmissionType = transmissionType;
    }
}
