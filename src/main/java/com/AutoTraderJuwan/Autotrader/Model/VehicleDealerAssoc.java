package com.AutoTraderJuwan.Autotrader.Model;

public class VehicleDealerAssoc {
    private int VDAId;
    private int VehicleDId;
    private int DealerId;

    public int getVDAId() {
        return VDAId;
    }

    public void setVDAId(int VDAId) {
        this.VDAId = VDAId;
    }

    public int getVehicleDId() {
        return VehicleDId;
    }

    public void setVehicleDId(int vehicleDId) {
        VehicleDId = vehicleDId;
    }

    public int getDealerId() {
        return DealerId;
    }

    public void setDealerId(int dealerId) {
        DealerId = dealerId;
    }

    public VehicleDealerAssoc(int VDAId, int vehicleDId, int dealerId) {
        this.VDAId = VDAId;
        VehicleDId = vehicleDId;
        DealerId = dealerId;
    }
}
