package com.AutoTraderJuwan.Autotrader.Model;

public class DealerUserAssoc {
    private int DUAId;
    private int UserId;
    private int DealerId;

    public int getDUAId() {
        return DUAId;
    }

    public void setDUAId(int DUAId) {
        this.DUAId = DUAId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        this.UserId = userId;
    }

    public int getDealerId() {
        return DealerId;
    }

    public void setDealerId(int dealerId) {
        this.DealerId = dealerId;
    }

    public DealerUserAssoc(int DUAId, int userId, int dealerId) {
        this.DUAId = DUAId;
        this.UserId = userId;
        this.DealerId = dealerId;
    }
}
