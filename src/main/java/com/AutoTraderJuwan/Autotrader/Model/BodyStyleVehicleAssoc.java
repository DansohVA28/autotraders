package com.AutoTraderJuwan.Autotrader.Model;

public class BodyStyleVehicleAssoc {
    private int BodyStyleVehicleId;
    private int BodyStyleId;
    private int VehicleDId;

    public int getBodyStyleVehicleId() {
        return BodyStyleVehicleId;
    }

    public void setBodyStyleVehicleId(int bodyStyleVehicleId) {
        this.BodyStyleVehicleId = bodyStyleVehicleId;
    }

    public int getBodyStyleId() {
        return BodyStyleId;
    }

    public void setBodyStyleId(int bodyStyleId) {
        this.BodyStyleId = bodyStyleId;
    }

    public int getVehicleDId() {
        return VehicleDId;
    }

    public void setVehicleDId(int vehicleDId) {
        this.VehicleDId = vehicleDId;
    }

    public BodyStyleVehicleAssoc(int bodyStyleVehicleId, int bodyStyleId, int vehicleDId) {
        this.BodyStyleVehicleId = bodyStyleVehicleId;
        this.BodyStyleId = bodyStyleId;
        this.VehicleDId = vehicleDId;
    }
}
